/*
 * ElevatorControl.h
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */

#ifndef ELEVATORCONTROLLER_H_
#define ELEVATORCONTROLLER_H_

#include <queue>
#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>

#include "../Elevator/Elevator.h"
#include "../CallButton/CallButton.h"

using namespace std;

enum ELEVATOR_DIRECTION {
	STOPPED,
	UPWARDS,
	DOWNWARDS
};


struct DestinationNode {
	int dest_floor;
	DestinationNode* next;
};

//Destination List is a singly linked-list that is used to store the destination floors
class DestinationList {

	DestinationNode* head;
	int length;
public:
	DestinationList() {
		this->head = NULL;
		this->length = 0;
	}

	~DestinationList() {
		DestinationNode* cur = this->head;
		while(cur != NULL)
		{
			DestinationNode* next = cur->next;
			delete cur;
			cur = next;
		}
	}

	void AddDestination(int dest_floor) {

		//if duplicate value or zero, don't add to list
		if(!DuplicateCheck(dest_floor) || dest_floor == 0)
		{
			return;
		}

		DestinationNode* newnode = new DestinationNode;
		newnode->dest_floor = dest_floor;
		newnode->next = NULL;

		if(this->head== NULL)
		{
			this->head = newnode;
		}
		else {
			DestinationNode* cur = this->head;
			while( cur->next!= NULL)
			{
				cur = cur->next;
			}
			cur->next = newnode;
		}
		this->length++;
	}

	/*
	 * Sort Algortithm searches for the next destination by checking the elvator direction and going to the closest floor in that direction
	 * While there is probably a more efficient algorithm, this prevents the elevator from starving floors from being serviced. --SAW
	 */
	void SortNextDestination(int cur_floor, int max_floor, ELEVATOR_DIRECTION dir) {
		if(this->head == NULL)
			return;

		if(dir == UPWARDS)
		{
			for(int i = cur_floor+1; i <= max_floor; i++)
			{
				DestinationNode* cur = this->head;
				while(cur != NULL)
				{
					if(cur->dest_floor == i)
					{
						cur->dest_floor = this->head->dest_floor;
						this->head->dest_floor = i;
						return;
					}
					cur=cur->next;
				}
			}

			for(int i = cur_floor-1; i >= 1; i--)
			{
				DestinationNode* cur = this->head;
				while(cur != NULL)
				{
					if(cur->dest_floor == i)
					{
						cur->dest_floor = this->head->dest_floor;
						this->head->dest_floor = i;
						return;
					}
					cur=cur->next;
				}
			}
		}
		else if (dir == DOWNWARDS)
		{
			for(int i = cur_floor-1; i >= 1; i--)
			{
				DestinationNode* cur = this->head;
				while(cur != NULL)
				{
					if(cur->dest_floor == i)
					{
						cur->dest_floor = this->head->dest_floor;
						this->head->dest_floor = i;
						return;
					}
					cur=cur->next;
				}
			}

			for(int i = cur_floor+1; i <= max_floor; i++)
			{
				DestinationNode* cur = this->head;
				while(cur != NULL)
				{
					if(cur->dest_floor == i)
					{
						cur->dest_floor = this->head->dest_floor;
						this->head->dest_floor = i;
						return;
					}
					cur=cur->next;
				}
			}
		}
	}

	bool isEmpty() {
		return this->head == NULL;
	}

	bool PeekFirst(int* i_out) {
		if(this->head == NULL)
			return false;
		else
			*i_out = this->head->dest_floor;
		return true;
	}

	bool RemoveFirst() {
		if(this->head == NULL)
			return false;
		else
		{
			DestinationNode * temp = this->head;
			this->head = this->head->next;
			delete temp;
		}
		this->length--;
		return true;
	}

private:
	//removes node based on destination floor
	void RemoveDestination(int new_val) {
		DestinationNode* cur = this->head, *prev = NULL;
		while(cur != NULL)
		{
			if(cur->dest_floor == new_val)
			{
				if(prev == NULL)
				{
					this->head = cur->next;

				}
				else
				{
					prev->next = cur->next;
				}
				delete cur;
			}
			else
			{
				prev = cur;
				cur = cur->next;
			}
		}
	}

	//checks to see if a floor is already an existing value in the list
	bool DuplicateCheck(int new_val) {
		DestinationNode* cur = this->head;
		while(cur != NULL)
		{
			if(new_val == cur->dest_floor)
				return false;
			cur = cur->next;
		}

		return true;
	}
};

class ElevatorController {

private:
	Elevator e;
	ELEVATOR_DIRECTION direction;
	CallButton* buttons;

	DestinationList* dest_list;
	int cur_dest;
private:
	mutex mtx;
	thread con_thread;
	bool term_thread_flag;



public:
	ElevatorController(Elevator e_in);
	virtual ~ElevatorController();

	void StartElevator();
	void ServiceElevator();
	bool CallElevator(int source_floor, int* dest_floors, int dest_floors_cnt);

	void PrintSummary();

	void StartControllerThread();
	void KillControllerThread();
private:
	void ControllerThreadFunc();
	bool ElevatorErrorCheck();
};

#endif /* ELEVATORCONTROLLER_H_ */
