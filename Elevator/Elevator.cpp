/*
 * Elevator.cpp
 *
 *  Created on: Feb 12, 2018
 *      Author: swalmsley
 */

#include "Elevator.h"

Elevator::Elevator(int max_floor) {
	this->MAX_FLOORS = max_floor;
	this->cur_floor = 1;
	this->status = ELEVATOR_OUT_OF_ORDER;
	this->door_status = ELEVATOR_DOORS_CLOSED;
}

Elevator::~Elevator() {
	// nothing to clean up
}

bool Elevator::MoveElevatorUp() {
	if(this->cur_floor == this->MAX_FLOORS)
	{
		cout << "ERROR: Elevator cannot move higher than floor!" << this->cur_floor << endl;
		return false;
	}

	this->cur_floor++;
	return true;
}

bool Elevator::MoveElevatorDown() {
	if(this->cur_floor == 1)
	{
		cout << "ERROR: Elevator cannot move lower than 1st floor!" << endl;
		return false;
	}
	this->cur_floor--;
	return true;
}

int Elevator::GetCurrentFloor() {
	return this->cur_floor;
}

int Elevator::GetMaxFloor() {
	return this->MAX_FLOORS;
}

bool Elevator::OpenElevatorDoors() {
	cout << "Opening Elevator Doors" << endl;
	this->door_status = ELEVATOR_DOORS_OPEN;
	return true;
}

bool Elevator::CloseElevatorDoors() {
	cout << "Closing Elevator Doors" << endl;
	this->door_status = ELEVATOR_DOORS_CLOSED;
	return true;
}

ELEVATOR_DOOR_STATUS Elevator::GetElevatorDoorStatus() {
	return this->door_status;
}

void Elevator::SetElevatorInService() {
	this->status = ELEVATOR_IN_SERVICE;
}

void Elevator::SetElevatorOutOfOrder() {
	this->status = ELEVATOR_OUT_OF_ORDER;
}

ELEVATOR_STATUS Elevator::GetElevatorStatus() {
	return this->status;
}
